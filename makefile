# Web Device Information
# Copyright (C) 2014-2015, Nicola Spanti (also known as RyDroid) <dev@nicola-spanti.info>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# Directories and files
SRC_DIR=src
BUILD_DIR=build
DOC_DIR=doc
JAVA_DIR=$(SRC_DIR)/java
FILES_FOR_APP=audio/ css/ html/ images/ js/ video/ favicon.ico index.html
FILES_FOR_ARCHIVE=src/ makefile LICENCE*

# Commands
RM=rm -f
JAVAC=javac


.PHONY: \
	$(DOC_DIR) list rm-builds clean all \
	all-archives archive-default \
	archive-zip archive-tar-gz archive-tar-bz2 archive-tar-xz archive-7z \
	all-apps website firefox-app chromium-app


# List of source files
list:
	ls -- $(SRC_DIR)/

# Remove the builds
rm-builds:
	$(RM) -r -- $(BUILD_DIR)/

clean: rm-builds

all: archive-default all-apps


all-archives: \
	archive-tar-gz archive-tar-bz2 archive-tar-xz \
	archive-zip archive-7z

archive-default: archive-tar-xz

# Zip archive and compression of the source code
archive-zip: $(BUILD_DIR)/source.zip
$(BUILD_DIR)/source.zip: $(FILES_FOR_ARCHIVE)
	@mkdir -p $(BUILD_DIR)/
	zip $(BUILD_DIR)/source.zip -r -- $(FILES_FOR_ARCHIVE)

# Tar archive of the source code with gz compression
archive-tar-gz: $(BUILD_DIR)/source.tar.gz
$(BUILD_DIR)/source.tar.gz: $(FILES_FOR_ARCHIVE)
	@mkdir -p $(BUILD_DIR)/
	tar -zcvf $(BUILD_DIR)/source.tar.gz -- $(FILES_FOR_ARCHIVE)

# Tar archive of the source code with bz2 compression
archive-tar-bz2: $(BUILD_DIR)/source.tar.bz2
$(BUILD_DIR)/source.tar.bz2: $(FILES_FOR_ARCHIVE)
	@mkdir -p $(BUILD_DIR)/
	tar -jcvf $(BUILD_DIR)/source.tar.bz2 -- $(FILES_FOR_ARCHIVE)

# Tar archive of the source code with xz compression
archive-tar-xz: $(BUILD_DIR)/source.tar.xz
$(BUILD_DIR)/source.tar.xz: $(FILES_TO_ARCHIVE)
	@mkdir -p $(BUILD_DIR)/
	tar -cJvf $(BUILD_DIR)/source.tar.xz -- $(FILES_FOR_ARCHIVE)

# 7z archive of the source code
archive-7z: $(BUILD_DIR)/source.7z
$(BUILD_DIR)/source.7z: $(FILES_TO_ARCHIVE)
	@mkdir -p $(BUILD_DIR)/
	7z a -t7z $(BUILD_DIR)/source.7z $(FILES_FOR_ARCHIVE)


# Packages for all supported "platforms"
all-apps: website firefox-app chromium-app

# Files for hosting the app on the website
website: $(SRC_DIR)/
	@mkdir -p $(BUILD_DIR)/website/
	cd $(SRC_DIR)/; \
		cp -r -- $(FILES_FOR_APP) .htaccess manifest.webapp ../$(BUILD_DIR)/website/

# Package for Firefox/Iceweasel/IceCat and Boot 2 Gecko (non brand name of Firefox OS)
firefox-app: $(SRC_DIR)/
	@mkdir -p $(BUILD_DIR)/
	cd $(SRC_DIR)/; \
		zip firefox-app.zip -r -- $(FILES_FOR_APP) manifest.webapp
	mv $(SRC_DIR)/firefox-app.zip $(BUILD_DIR)/

# Package for Chromium and the non-free Google Chrome
# manifest notes:
# * Chromium wants a .json, not a .webapp
# * defaut_locale is used by Chromium for his non standard i18n and a _locales/ is needed with it
chromium-app: $(SRC_DIR)/
	@mkdir -p $(BUILD_DIR)/
	grep -v default_locale $(SRC_DIR)/manifest.webapp > $(SRC_DIR)/manifest.json
	cd $(SRC_DIR)/; \
		zip chromium-app.zip -r -- $(FILES_FOR_APP) manifest.json
	rm -f $(SRC_DIR)/manifest.json
	mv $(SRC_DIR)/chromium-app.zip $(BUILD_DIR)/

# Package for Android (and the 100% free/libre fork Replicant)
#android-app:
#	TODO

# Packages for GNU/Linux
# deb rpm
# TODO

# Package for Ubuntu
# TODO
# https://developer.ubuntu.com/publish/webapp/packaging-web-apps/

# Package for Tizen
# TODO

# Package for Windows and ReactOS
# TODO

# Package for OS X
# TODO

# Package for JVM (Java Virtual Machine)
#jar-app: $(SRC_DIR)/
#	@mkdir -p $(BUILD_DIR)/
#	cd $(JAVA_DIR) && $(JAVAC) WebDeviceInformation.java
#	cd $(JAVA_DIR) && jar -cmf manifest.mf web-device-information.jar *.class ../index.html ../html/ ../css/ ../js/ ../images/ ../audio/ ../video/
#	TOFIX Excepted *.class, packaged files are not used and the jar must be in $(JAVA_DIR)!
#	mv $(JAVA_DIR)/web-device-information.jar $(BUILD_DIR)/web-device-information.jar
#	$(RM) $(JAVA_DIR)/*.class

# Documentation
#$(DOC_DIR):
#	TODO
