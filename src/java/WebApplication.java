/*
 * Web Device Information
 * Copyright (C) 2015, Nicola Spanti (also known as RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


import java.net.URI;
import java.nio.file.Paths;
import javafx.scene.Scene;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.application.Application;

/**
 * Web application
 */
public class WebApplication extends Application
{
	public final WebView webView = new WebView();
	public final Scene scene = new Scene(webView);
	
	
	public WebApplication(final URI uri)
	{
		super();
		this.webView.getEngine().load(uri.toString());
	}
	
	public WebApplication(final String path)
	{
		this(Paths.get(path).toAbsolutePath().toUri());
	}
	
	
	@Override
	public void start(final Stage stage)
	{
		stage.setScene(this.scene);
		//stage.setTitle(this.webView.getEngine().getTitle()); // does not work :(
		stage.show();
	}
}
