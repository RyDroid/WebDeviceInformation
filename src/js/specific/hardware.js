/*
 * Web Device Information
 * Copyright (C) 2014-2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/* Requires
 * string-functions (String.prototype.trim isString)
 * gamepad-core-functions
 * gamepad-buttons-functions
 * gamepad-print-functions.js
 */


var futureInnerHTML = '';

if(screen)
{
	futureInnerHTML += '<h2 id="screen">Screen</h2>';
	futureInnerHTML += '<img src="../images/devices/video-display.svg" alt="" class="float-right display-min-width-700px" />';
	futureInnerHTML += '<ul>';
	if(screen.height)
		futureInnerHTML += '<li>Height: '+ screen.height +' pixels</li>';
	if(screen.width)
		futureInnerHTML += '<li>Width: '+ screen.width +' pixels</li>';
	if(screen.height && screen.width)
		futureInnerHTML += '<li>Number of pixels: '+ (screen.height * screen.width) +'</li>';
	if(screen.colorDepth)
		futureInnerHTML += '<li>Color depth: '+ screen.colorDepth +'</li>';
	if(screen.orientation)
		futureInnerHTML += '<li id="orientation">No information.</li>';
	futureInnerHTML += '</ul>';
}

if(window.battery)
	var battery = window.battery;
else if(navigator.battery)
	var battery = navigator.battery;
if(battery)
{
	futureInnerHTML += '<h2 id="battery">Battery</h2>';
	futureInnerHTML += '<img src="../images/devices/battery.svg" alt="" class="float-right display-min-width-700px" />';
	futureInnerHTML += '<ul>';
	if(battery.charging)
		futureInnerHTML += '<li id="charging"></li>';
	if(battery.chargingTime)
		futureInnerHTML += '<li id="chargingTime"></li>';
	if(battery.dischargingTime)
		futureInnerHTML += '<li id="dischargingTime"></li>';
	if(battery.level)
		futureInnerHTML += '<li id="batteryLevel">Level of charging: '+ (battery.level*100) +' / 100</li>';
	futureInnerHTML += '</ul>';
}

/**
 * @param {unsigned integer} number of seconds
 * @param {boolean} string with a title attribute for abbreviations or not?
 * @return {string} a human readable string corresponding to the "number of seconds" parameter
 */
function secondsToHumanReadableString(seconds, withTitleAttributeHelper)
{
	if(withTitleAttributeHelper == undefined)
		withTitleAttributeHelper = true;
	
	function getAbbrWithHelperOrNot(abbr, helper)
	{
		if(withTitleAttributeHelper)
			return '<abbr title="'+ helper +'">'+ abbr +'</abbr>';
		return abbr;
	}
	
	if(isNaN(seconds))
		throw new TypeError('seconds must be a number');
	if(seconds < 0)
		throw new RangeError('seconds must be greater than or equal to 0');
	seconds = parseInt(seconds, 10);
	var humanReadableString = '';
	
	if(seconds >= 60)
	{
		var minutes = Math.floor(seconds / 60);
		seconds -= minutes * 60;
		
		if(minutes >= 60)
		{
			var hours = Math.floor(minutes / 60);
			minutes -= hours * 60;
			
			if(hours >= 60)
			{
				var days = Math.floor(hours / 24);
				hours -= days * 24;
				
				humanReadableString += days + getAbbrWithHelperOrNot('d', days > 1 ? 'days' : 'day') +' ';
			}
			
			humanReadableString += hours + getAbbrWithHelperOrNot('h', hours > 1 ? 'hours' : 'hour') +' ';
		}
		
		humanReadableString += minutes + getAbbrWithHelperOrNot('min', minutes > 1 ? 'minutes' : 'minute') +' ';
	}
	
	humanReadableString += seconds + getAbbrWithHelperOrNot('s', seconds > 1 ? 'seconds' : 'second') +' ';
	return humanReadableString;
}

futureInnerHTML += '<h2 id="gamepads">Gamepads</h2>';
futureInnerHTML += '<img src="../images/devices/input-gaming.svg" alt="" class="float-right display-min-width-700px" />';
futureInnerHTML += '<span id="gamepads-content"><p>There is no information about gamepads.</p></span>';

window.addEventListener('load', function()
{
	var body = document.querySelector('main');
	body.innerHTML = futureInnerHTML;
	
	if(screen && screen.orientation)
	{
		function updateOrientation(event)
		{
			var element = document.getElementById('orientation');
			if(element)
			{
				var futureInnerHTML = 'Orientation: ';
				
				var orientation = screen.orientation;
				if(typeof(ScreenOrientation) == 'function' && orientation instanceof ScreenOrientation && isString(orientation.type))
					orientation = orientation.type;
				if(isString(orientation))
				{
					orientation = orientation.trim().toLowerCase();
					
					if(orientation == 'default')
						futureInnerHTML += 'default';
					else if(orientation.contains('portrait'))
						futureInnerHTML += 'portrait';
					else if(orientation.contains('landscape'))
						futureInnerHTML += 'landscape';
					
					if(orientation.contains('primary'))
						futureInnerHTML += ' (forward)';
					else if(orientation.contains('secondary'))
						futureInnerHTML += ' (backward)';
					
					if(typeof(DeviceOrientationEvent) == 'function' && event instanceof DeviceOrientationEvent)
					{
						futureInnerHTML += ' (';
						if(event.absolute != undefined)
						{
							if(!event.absolute)
								futureInnerHTML += 'not ';
							futureInnerHTML += 'absolute, ';
						}
						futureInnerHTML += 'x='+ event.beta +'°, y='+ event.gamma +'°, z='+ event.alpha +'°';
						futureInnerHTML += ')';
					}
					
					element.innerHTML = futureInnerHTML;
				}
			}
		}
		updateOrientation();
		
		if(screen.addEventListener)
			screen.addEventListener('orientationchange', updateOrientation);
	}
	
	if(battery)
	{
		function updateChargingStatus()
		{
			var element = document.getElementById('charging');
			if(element)
				element.innerHTML = 'Charging: '+ (battery.charging ? 'yes' : 'no');
		}
		updateChargingStatus();
		
		function updateChargingTimeStatus()
		{
			var element = document.getElementById('chargingTime');
			if(element)
			{
				element.hidden = battery.chargingTime == Infinity;
				if(!element.hidden)
					element.innerHTML = 'Time before complete charging: '+ secondsToHumanReadableString(battery.chargingTime);
			}
		}
		updateChargingTimeStatus();
		
		function updateDischargingTimeStatus()
		{
			var element = document.getElementById('dischargingTime');
			if(element)
			{
				element.hidden = battery.dischargingTime == Infinity;
				if(!element.hidden)
					element.innerHTML = 'Time before complete discharging: '+ secondsToHumanReadableString(battery.dischargingTime);
			}
		}
		updateDischargingTimeStatus();
		
		function updateBatteryLevelStatus()
		{
			var element = document.getElementById('batteryLevel');
			if(element)
				element.innerHTML = 'Level of charging: '+ parseInt(battery.level*100, 10) +' / 100';
		}
		updateBatteryLevelStatus();
		
		battery.addEventListener('chargingchange',        updateChargingStatus);
		battery.addEventListener('chargingtimechange',    updateChargingTimeStatus);
		battery.addEventListener('dischargingtimechange', updateDischargingTimeStatus);
		battery.addEventListener('levelchange',           updateBatteryLevelStatus);
	}
	
	if(typeof(navigator.getGamepads) == 'function' && typeof(navigator.getGamepads().length) == 'number')
	{
		function printGamepads()
		{
			'use strict';
			var element = document.getElementById('gamepads-content');
			if(element)
				element.innerHTML = gamepadsToHTMLString(getGamepads());
		}
		
		printGamepads();
		
		var gamepadRefreshInterval = null;
		
		function gamepadConnected(gamepadEvent)
		{
			printGamepads();
			
			if(isGamepad(gamepadEvent.gamepad) && typeof(Notification) == 'function')
			{
				if (Notification.permission === 'granted')
				{
					gamepadConnectedNotification(gamepadEvent.gamepad);
				}
				else if (Notification.permission !== 'denied')
				{
					Notification.requestPermission(function (permission) {
						if (permission === 'granted')
							gamepadConnectedNotification(gamepadEvent.gamepad);
					});
				}
			}
			
			if(gamepadRefreshInterval == null)
			{
				gamepadRefreshInterval = setInterval(printGamepads, 200);
			}
		}
		
		function gamepadDisconnected(gamepadEvent)
		{
			printGamepads();
			
			if(isGamepad(gamepadEvent.gamepad) && typeof(Notification) == 'function')
			{
				if (Notification.permission === 'granted')
				{
					gamepadDisconnectedNotification(gamepadEvent.gamepad);
				}
				else if (Notification.permission !== 'denied')
				{
					Notification.requestPermission(function (permission) {
						if (permission === 'granted')
							gamepadDisconnectedNotification(gamepadEvent.gamepad);
					});
				}
			}
			
			if(getGamepads().length == 0)
			{
				clearInterval(gamepadRefreshInterval);
				gamepadRefreshInterval = null;
			}
		}
		
		window.addEventListener('gamepadconnected',    gamepadConnected);
		window.addEventListener('gamepaddisconnected', gamepadDisconnected);
		if(!isGamepadArrayEmpty(getGamepads()))
		{
			gamepadRefreshInterval = setInterval(printGamepads, 50);
		}
	}
});
