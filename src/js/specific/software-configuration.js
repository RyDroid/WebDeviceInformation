/*
 * Web Device Information
 * Copyright (C) 2014, 2016  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/* Requires
 * string-functions (String.prototype.trim isString)
 * array-functions (Array.isArray)
 */


var futureInnerHTML = '';

futureInnerHTML += '<ul>';
if(navigator)
{
	if(navigator.onLine)
		futureInnerHTML += '<li id="online"></li>';
	
	if(typeof(navigator.cookieEnabled) == 'boolean')
		futureInnerHTML += '<li id="cookies">Cookies enabled: '+ (navigator.cookieEnabled ? 'yes' : 'no') +'</li>';
	else if(typeof(navigator.cookieEnabled) == 'function')
		futureInnerHTML += '<li id="cookies">Cookies enabled: '+ (navigator.cookieEnabled() ? 'yes' : 'no') +'</li>';
	
	if(navigator.doNotTrack)
		futureInnerHTML += '<li id="dnt">Do Not Track: '+ (navigator.doNotTrack ? 'yes' : 'no') +'</li>';
	
	if(navigator.languages && Array.isArray(navigator.languages) && navigator.languages.length > 0)
		futureInnerHTML += '<li id="lang">Languages: '+ navigator.languages.join(', ') +'</li>';
	else if(navigator.language && navigator.language.trim().length > 0)
		futureInnerHTML += '<li id="lang">Language: '+ navigator.language.trim() +'</li>';
	
	if(navigator.oscpu && navigator.oscpu.trim().length > 0)
		futureInnerHTML += '<li id="oscpu"><abbr title="Operating System">OS</abbr> <abbr title="Central Processing Unit">CPU</abbr>: '+ navigator.oscpu.trim() +'</li>';
	
	if(navigator.platform && navigator.platform.trim().length > 0)
		futureInnerHTML += '<li id="platform">Platform: '+ navigator.platform.trim() +'</li>';
	
	if(navigator.appName && navigator.appName.trim().length > 0)
		futureInnerHTML += '<li id="appName">Web browser name: '+ navigator.appName.trim() +'</li>';
	
	if(navigator.appCodeName && navigator.appCodeName.trim().length > 0)
		futureInnerHTML += '<li id="appCodeName">Web browser code name: '+ navigator.appCodeName.trim() +'</li>';
	
	if(navigator.product && navigator.product.trim().length > 0)
		futureInnerHTML += '<li id="productName">Web browser product name: '+ navigator.product.trim() +'</li>';
	
	if(navigator.appVersion && navigator.appVersion.trim().length > 0)
		futureInnerHTML += '<li id="appVersion">Web browser version: '+ navigator.appVersion.trim() +'</li>';
	
	if(navigator.buildID && navigator.buildID.trim().length > 0)
		futureInnerHTML += '<li id="buildID">Web browser build identifier: '+ navigator.buildID.trim() +'</li>';
	
	if(navigator.vendor && navigator.vendor.trim().length > 0)
	{
		futureInnerHTML += '<li id="vendor">Vendor: '+ navigator.vendor.trim();
		if(navigator.vendorSub && navigator.vendorSub.trim().length > 0)
			futureInnerHTML += ' (version '+ navigator.vendorSub.trim() +')';
		futureInnerHTML += '</li>';
	}
	
	if(navigator.userAgent && navigator.userAgent.trim().length > 0)
		futureInnerHTML += '<li id="user-agent">User-agent: '+ navigator.userAgent.trim() +'</li>';
	
	if(typeof(navigator.javaEnabled) == 'function')
		futureInnerHTML += '<li id="java">Java enabled: '+ (navigator.javaEnabled() ? 'yes' : 'no') +'</li>';
	else if(typeof(navigator.javaEnabled) == 'boolean')
		futureInnerHTML += '<li id="java">Java enabled: '+ (navigator.javaEnabled ? 'yes' : 'no') +'</li>';
	
	if(window.fullScreen)
		futureInnerHTML += '<li id="fullscreen"></li>';
	
	if(typeof(navigator.vibrate) == 'function')
		futureInnerHTML += '<li id="vibrate">Vibration support: yes, <a id="vibrate-try">try</a></li>';
	
	if(typeof(Notification) == 'function' && Notification.permission)
		futureInnerHTML += '<li id="notification">Notification: '+ Notification.permission +', <a id="notification-try">try</a></li>';
}
futureInnerHTML += '</ul>';

futureInnerHTML += '<h2 id="geolocation">Geolocation</h2>';
futureInnerHTML += '<span id="geolocation-content"><p>Position is not available.</p></span>';

if(navigator.plugins && typeof(navigator.plugins.length) != 'undefined')
{
	futureInnerHTML += '<h2 id="plugins">Plugins</h2>';
	
	
	if(navigator.plugins.length == 0)
	{
		futureInnerHTML += '<p>None.</p>';
	}
	else
	{
		futureInnerHTML += '<table>';
		
		var nameColumnExists = isString(navigator.plugins[0].name);
		var descriptionColumnExists = isString(navigator.plugins[0].description);
		var versionColumnExists = isString(navigator.plugins[0].version);
		var filenameColumnExists = isString(navigator.plugins[0].filename);
		var mimeTypesColumnExists;
		try
		{
			mimeTypesColumnExists = typeof(navigator.plugins[0].item) == 'function' && (Array.isArray(navigator.plugins[0].item()) && navigator.plugins[0].item()[0] instanceof MimeType);
		}
		catch(e1)
		{
			try
			{
				mimeTypesColumnExists = typeof(navigator.plugins[0].item) == 'function' && (Array.isArray(navigator.plugins[0].item()) && navigator.plugins[0].item(0) instanceof MimeType);
			}
			catch(e2)
			{
				mimeTypesColumnExists = false;
			}
		}
		var mimeTypeColumnExists;
		if(mimeTypesColumnExists)
		{
			mimeTypeColumnExists = false;
		}
		else
		{
			try
			{
				mimeTypeColumnExists = typeof(navigator.plugins[0].item) == 'function' && navigator.plugins[0].item() instanceof MimeType;
			}
			catch(e1)
			{
				try
				{
					mimeTypeColumnExists = typeof(navigator.plugins[0].item) == 'function' && navigator.plugins[0].item(0) instanceof MimeType;
				}
				catch(e2)
				{
					mimeTypeColumnExists = false;
				}
			}
		}
		
		futureInnerHTML += '<thead><tr>';
		if(nameColumnExists)
			futureInnerHTML += '<th>Name</th>';
		if(descriptionColumnExists)
			futureInnerHTML += '<th>Description</th>';
		if(versionColumnExists)
			futureInnerHTML += '<th>Version</th>';
		if(filenameColumnExists)
			futureInnerHTML += '<th>Filename</th>';
		if(mimeTypesColumnExists)
			futureInnerHTML += '<th><abbr title="Multi-Purpose Internet Mail Extensions">MIME</abbr> types</th>';
		else if(mimeTypeColumnExists)
			futureInnerHTML += '<th><abbr title="Multi-Purpose Internet Mail Extensions">MIME</abbr> type</th>';
		futureInnerHTML += '</tr></thead>';
		
		function mimeTypeToHTMLListString(mimeType)
		{
			if(!(mimeType instanceof MimeType))
				throw new TypeError('mimeType is needed');
			
			var string = '';
			if(isString(mimeType.description) && mimeType.description.trim().length > 0)
				string += '<li>Description: '+ mimeType.description.trim() +'</li>';
			if(isString(mimeType.type) && mimeType.type.trim().length > 0)
				string += '<li>Type: '+ mimeType.type.trim() +'</li>';
			if(Array.isArray(mimeType.suffixes) && mimeType.suffixes.length > 0 && isString(mimeType.suffixes[0]) && mimeType.suffixes[0].trim().length > 0)
				string += '<li>Suffixes: '+ mimeType.suffixes.join(' ') +'</li>';
			else if(isString(mimeType.suffixes) && mimeType.suffixes.length > 0)
				string += '<li>Suffixes: '+ mimeType.suffixes +'</li>';
			else if(isString(mimeType.suffixe) && mimeType.suffixe.trim().length > 0)
				string += '<li>Suffixe: '+ mimeType.suffixe.trim() +'</li>';
			return '<ul>' + string + '</ul>';
		}
		
		futureInnerHTML += '<tbody>';
		for(var i=0; i < navigator.plugins.length; ++i)
		{
			futureInnerHTML += '<tr>';
			if(nameColumnExists)
				futureInnerHTML += '<td>'+ navigator.plugins[i].name.trim() +'</td>';
			if(descriptionColumnExists)
				futureInnerHTML += '<td>'+ navigator.plugins[i].description.trim() +'</td>';
			if(versionColumnExists)
				futureInnerHTML += '<td>'+ navigator.plugins[i].version.trim() +'</td>';
			if(filenameColumnExists)
				futureInnerHTML += '<td>'+ navigator.plugins[i].filename.trim() +'</td>';
			if(mimeTypesColumnExists)
			{
				futureInnerHTML += '<td>';
				var mimeTypes = navigator.plugins[i].item();
				for(var j=0; j < mimeTypes.length; ++j)
					futureInnerHTML += mimeTypeToHTMLListString(mimeTypes[j]);
				futureInnerHTML += '</td>';
			}
			else if(mimeTypeColumnExists)
				futureInnerHTML += '<td>'+ mimeTypeToHTMLListString(navigator.plugins[i].item()) +'</td>';
			futureInnerHTML += '</tr>';
		}
		futureInnerHTML += '</tbody>';
		
		futureInnerHTML += '</table>';
	}
}

window.addEventListener('load', function()
{
	var body = document.querySelector('main');
	body.innerHTML = futureInnerHTML;
	
	function updateOnLineStatus()
	{
		var element = document.getElementById('online');
		if(element)
			element.innerHTML = 'Online : '+ (navigator.onLine ? 'yes' : 'no');
	}
	updateOnLineStatus();
	
	function updateFullScreenStatus()
	{
		var element = document.getElementById('fullscreen');
		if(element)
			element.innerHTML = 'Full screen: '+ (window.fullScreen ? 'yes' : 'no');
	}
	updateFullScreenStatus();
	
	if(typeof(navigator.vibrate) == 'function')
	{
		var element = document.getElementById('vibrate-try');
		if(element)
		{
			element.addEventListener('click', function() {
				navigator.vibrate(200);
			});
		}
	}
	
	if(typeof(Notification) == 'function')
	{
		var element = document.getElementById('notification-try');
		if(element)
		{
			element.addEventListener('click', function() {
				function newNotification()
				{
					/** const */ var NOTIFICATION_ICON_PATH = '../favicon.ico';
					var notification = new Notification('Hi there!', {'icon': NOTIFICATION_ICON_PATH, 'body': '"Hi" by a web app!'});
					
					notification.onclose = function()
					{
						var notification = new Notification('Closing notification', {'icon': NOTIFICATION_ICON_PATH, 'body': 'A previous notification was closed'});
					}
				}
				
				if (Notification.permission === 'granted')
				{
					newNotification();
				}
				else if(Notification.permission !== 'denied' && typeof(Notification.requestPermission) == 'function')
				{
					Notification.requestPermission(function (permission) {
						if (permission === 'granted')
							newNotification();
					});
				}
			});
		}
	}
	
	if(typeof(Geolocation) != 'function' || !(navigator.geolocation instanceof Geolocation) || typeof(navigator.geolocation.getCurrentPosition) != 'function')
	{
		var element = document.getElementById('geolocation-content');
		if(element)
			element.innerHTML = '<p>Geolocation is not supported</p>';
	}
	else
	{
		var element = document.getElementById('geolocation-content');
		if(element)
		{
			function getCurrentPositionSuccess(position)
			{
				if(typeof(Position) == 'function' && position instanceof Position)
				{
					if(position.coords)
					{
						var elementFutureInnerHTML  = '';
						elementFutureInnerHTML += '<ul>';
						elementFutureInnerHTML += '<li>Latitude: '+ position.coords.latitude +'</li>';
						elementFutureInnerHTML += '<li>Longitude: '+ position.coords.longitude +'</li>';
						elementFutureInnerHTML += '<li>Altitude: '+ position.coords.altitude +'</li>';
						elementFutureInnerHTML += '<li>Accuracy: '+ position.coords.accuracy +'</li>';
						elementFutureInnerHTML += '<li>Altitude accuracy: '+ position.coords.altitudeAccuracy +'</li>';
						elementFutureInnerHTML += '<li>Heading: '+ position.coords.heading +'</li>';
						elementFutureInnerHTML += '<li>Speed: '+ position.coords.position +'</li>';
						elementFutureInnerHTML += '</ul>';
						element.innerHTML = elementFutureInnerHTML;
					}
					else
					{
						element.innerHTML = '<p>Coordinates are not available.</p>';
					}
				}
			}
			
			function getCurrentPositionError()
			{
				element.innerHTML = '<p>No position is available.</p>';
			}
			
			var geolocationOptions = { enableHighAccuracy: true };
			
			navigator.geolocation.getCurrentPosition(getCurrentPositionSuccess, getCurrentPositionError, geolocationOptions);
			if(typeof(navigator.geolocation.watchPosition) == 'function')
				navigator.geolocation.watchPosition(getCurrentPositionSuccess, getCurrentPositionError, geolocationOptions);
		}
	}
	
	if(window.addEventListener)
	{
		window.addEventListener('online',           updateOnLineStatus);
		window.addEventListener('offline',          updateOnLineStatus);
		window.addEventListener('fullscreenchange', updateFullScreenStatus);
	}
});
