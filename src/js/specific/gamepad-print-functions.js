/*
 * Web Device Information
 * Copyright (C) 2014-2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/** const */
var GAMEPAD_NOTIFICATION_ICON_PATH = '../images/devices/input-gaming.svg';

/**
 * @param {Gamepad} gamepad A gamepad
 * @return {Notification} A notification for a connected gamepad
 */
function gamepadConnectedNotification(gamepad)
{
	'use strict';
	if(isGamepad(gamepad) && typeof(Notification) == 'function' && Notification.permission === 'granted')
	{
		return new Notification(
			'New gamepad connected',
			{
				'icon': GAMEPAD_NOTIFICATION_ICON_PATH,
				'body': 'New gamepad connected at index '+ gamepad.index +': '+ gamepad.id +', '+ gamepad.buttons.length +' buttons, '+ gamepad.axes.length +' axes.'
			}
		);
	}
	return null;
}

/**
 * @param {Gamepad} gamepad A gamepad
 * @return {Notification} A notification for a disconnected gamepad
 */
function gamepadDisconnectedNotification(gamepad)
{
	'use strict';
	if(typeof(Gamepad) == 'function' && gamepad instanceof Gamepad && typeof(Notification) == 'function' && Notification.permission === 'granted')
	{
		return new Notification(
			'Gamepad disconnected',
			{
				'icon': GAMEPAD_NOTIFICATION_ICON_PATH,
				'body': 'Gamepad disconnected at index '+ gamepad.index +': '+ gamepad.id +', '+ gamepad.buttons.length +' buttons, '+ gamepad.axes.length +' axes.'
			}
		);
	}
	return null;
}

function gamepadButtonPressedStateToStringUnsafe(button)
{
	return button.pressed
		? 'Pressed: '+ (button.value * 100) +'%.'
		: 'Not pressed.';
}

/**
 * @param {GamepadButton} button A gamepad button
 * @return {string} A HTML list element string
 */
function gamepadButtonToHTMLListElementString(button)
{
	'use strict';
	return isGamepadButton(button)
		? '<li>'+ gamepadButtonPressedStateToStringUnsafe(button) +'</li>'
		: '';
}

/**
 * @param {Array} array Array of gamepad buttons
 * @param {boolean} ordered Must the list be ordered?
 * @return {string} A HTML list string
 */
function gamepadButtonsToHTMLListString(buttons, ordered, olStartValue)
{
	'use strict';
	
	if(typeof(Array) == 'function' && buttons instanceof Array && buttons.length > 0)
	{
		var string = '';
		
		for(var buttonNumber=0; buttonNumber < buttons.length; ++buttonNumber)
			string += gamepadButtonToHTMLListElementString(buttons[buttonNumber]);
		
		if(string.length > 0)
		{
			if(ordered)
			{
				return typeof(olStartValue) == 'undefined'
					? '<ol>'+ string +'</ol>'
					: '<ol start="'+ olStartValue +'">'+ string +'</ol>';
			}
			return '<ul>'+ string +'</ul>';
		}
	}
	return '';
}

/**
 * @param {Array} array Array of gamepad buttons
 * @return {string} A HTML unordered list string
 */
function gamepadButtonsToHTMLUnorderedListString(buttons, olStartValue)
{
	'use strict';
	return gamepadButtonsToHTMLListString(buttons, false, olStartValue);
}

/**
 * @param {Array} array Array of gamepad buttons
 * @return {string} A HTML ordered list string
 */
function gamepadButtonsToHTMLOrderedListString(buttons, olStartValue)
{
	'use strict';
	return gamepadButtonsToHTMLListString(buttons, true, olStartValue);
}

function gamepadButtonsToHTMLString(gamepad)
{
	if(isGamepadWithStandardMapping(gamepad))
	{
		var string = '';
		
		(function()
		{
			var buttons = getGamepadButtonsForLeftDirectionalPadUnsafe(gamepad);
			string += '<li>Left directional pad<ul>';
			string += '<li>Left:  '+ gamepadButtonPressedStateToStringUnsafe(buttons['left'])  +'</li>';
			string += '<li>Right: '+ gamepadButtonPressedStateToStringUnsafe(buttons['right']) +'</li>';
			string += '<li>Up:    '+ gamepadButtonPressedStateToStringUnsafe(buttons['up'])    +'</li>';
			string += '<li>Down:  '+ gamepadButtonPressedStateToStringUnsafe(buttons['down'])  +'</li>';
			string += '</ul></li>';
		})();
		
		(function()
		{
			var buttons = getGamepadButtonsForRightCirclesUnsafe(gamepad);
			string += '<li>Right circles<ul>';
			if(isXboxGamepadUnsafe(gamepad))
			{
				string += '<li>A: '+ gamepadButtonPressedStateToStringUnsafe(buttons['down'])  +'</li>';
				string += '<li>B: '+ gamepadButtonPressedStateToStringUnsafe(buttons['right']) +'</li>';
				string += '<li>X: '+ gamepadButtonPressedStateToStringUnsafe(buttons['left'])  +'</li>';
				string += '<li>Y: '+ gamepadButtonPressedStateToStringUnsafe(buttons['up'])    +'</li>';
			}
			else if(isPlayStationGamepadUnsafe(gamepad))
			{
				string += '<li>□: '+ gamepadButtonPressedStateToStringUnsafe(buttons['left'])  +'</li>';
				string += '<li>○: '+ gamepadButtonPressedStateToStringUnsafe(buttons['right']) +'</li>';
				string += '<li>△: '+ gamepadButtonPressedStateToStringUnsafe(buttons['up'])    +'</li>';
				string += '<li>❌: '+ gamepadButtonPressedStateToStringUnsafe(buttons['down'])  +'</li>';
			}
			else
			{
				string += '<li>Left:  '+ gamepadButtonPressedStateToStringUnsafe(buttons['left'])  +'</li>';
				string += '<li>Right: '+ gamepadButtonPressedStateToStringUnsafe(buttons['right']) +'</li>';
				string += '<li>Up:    '+ gamepadButtonPressedStateToStringUnsafe(buttons['up'])    +'</li>';
				string += '<li>Down:  '+ gamepadButtonPressedStateToStringUnsafe(buttons['down'])  +'</li>';
			}
			string += '</ul></li>';
		})();
		
		(function()
		{
			var triggers = getGamepadButtonsForTriggersUnsafe(gamepad);
			string += '<li>Triggers<ul>';
			string += '<li>Left' + gamepadButtonsToHTMLOrderedListString(triggers['left'],  1)  +'</li>';
			string += '<li>Right'+ gamepadButtonsToHTMLOrderedListString(triggers['right'], 1) +'</li>';
			string += '</ul></li>';
		})();
		
		(function()
		{
			var buttons = getGamepadButtonsForSticks(gamepad);
			string += '<li>Sticks<ul>';
			string += '<li>Left:  '+ gamepadButtonPressedStateToStringUnsafe(buttons['left'])  +'</li>';
			string += '<li>Right: '+ gamepadButtonPressedStateToStringUnsafe(buttons['right']) +'</li>';
			string += '</ul></li>';
		})();
		
		(function()
		{
			string += '<li>Others<ul>';
			string += '<li>Start: '+ gamepadButtonPressedStateToStringUnsafe(getGamepadButtonForStartUnsafe(gamepad)) +'</li>';
			if(isXboxGamepadUnsafe(gamepad))
			{
				string += '<li>Back: '+ gamepadButtonPressedStateToStringUnsafe(getGamepadButtonForSelectUnsafe(gamepad)) +'</li>';
			}
			else
			{
				string += '<li>Select: '+ gamepadButtonPressedStateToStringUnsafe(getGamepadButtonForSelectUnsafe(gamepad)) +'</li>';
			}
			string += '<li>Home:   '+ gamepadButtonPressedStateToStringUnsafe(getGamepadButtonForHomeUnsafe(gamepad)) +'</li>';
			string += '</ul></li>';
		})();
		
		return '<ul>'+ string +'</ul>';
	}
	return gamepadButtonsToHTMLOrderedListString(gamepad.buttons, 0);
}

/**
 * @param {Array} axes Axes
 * @return {string} A one line string with axes of a gamepad
 */
function gamepadAxesToOneLineString(axes)
{
	'use strict';
	return typeof(Array) == 'function' && axes instanceof Array && axes.length > 0
		? 'Axes position ('+ axes.join(', ') +')'
		: '';
}

/**
 * @param {Array} axes Axes
 * @return {string} A HTML list element string
 */
function gamepadAxesToHTMLListElementString(axes)
{
	'use strict';
	var string = gamepadAxesToOneLineString(axes);
	return string.length > 0
		? '<li>'+ string +'</li>'
		: '';
}

/**
 * @param {Gamepad} gamepad A gamepad
 * @return {string} A HTML list element string
 */
function gamepadToHTMLListElementString(gamepad)
{
	'use strict';
	
	if(!isGamepad(gamepad))
		return '';
	
	var string = '';
	
	if(isString(gamepad.id) && gamepad.id.trim().length > 0)
		string += '<li>Id: '+ gamepad.id.trim() +'</li>';
	
	if(typeof(gamepad.index) === 'number')
		string += '<li>Index: '+ gamepad.index +'</li>';
	
	if(typeof(gamepad.connected) == 'boolean')
		string += '<li>Connected: '+ (gamepad.connected ? 'yes' : 'no') +'</li>';
	
	if(isString(gamepad.mapping))
		string += '<li>Mapping: '+ (gamepad.mapping.trim().length == 0 ? 'none' : gamepad.mapping.trim()) +'</li>';
	
	if(isGamepadWithButtons(gamepad))
	{
		var stringTmp = gamepadButtonsToHTMLString(gamepad);
		if(stringTmp.length > 0)
			string += '<li>Buttons'+ stringTmp +'</li>';
	}
	
	if(gamepad.axes && typeof(gamepad.axes.length) != 'undefined' && gamepad.axes.length > 0)
		string += gamepadAxesToHTMLListElementString(gamepad.axes);
	
	if(gamepad.timestamp)
		string += '<li>Last time the data for this gamepad was updated: '+ gamepad.timestamp +'s</li>';
	
	return string.length > 0
		? '<li><ul>'+ string +'</ul></li>'
		: '';
}

/**
 * @param {Array} array Array of gamepads
 * @return {string} A HTML unordered list string
 */
function gamepadsToHTMLUnorderedListString(gamepads)
{
	'use strict';
	
	if(typeof(Gamepad) != 'function')
		return '';
	
	var string = '';
	
	for(var i=0; i < gamepads.length; ++i)
	{
		if(gamepads[i] instanceof Gamepad)
			string += gamepadToHTMLListElementString(gamepads[i]);
	}
	
	return string.length > 0
		? '<ul>'+ string +'</ul>'
		: '';
}

/**
 * @param {Array} array Array of gamepads
 * @return {string} A HTML string with information of the gamepads
 */
function gamepadsToHTMLString(gamepads)
{
	'use strict';
	return isGamepadArrayEmpty(gamepads)
		? '<p>None.</p>'
		: gamepadsToHTMLUnorderedListString(gamepads);
}
