/*
 * Web Device Information
 * Copyright (C) 2014  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


var body = document.querySelector('main');
var audioElement = document.createElement('audio');

if(typeof(audioElement.canPlayType) != 'function')
{
	body.innerHTML = '<p>Audio is not supported.</p>';
}
else
{
	var containers = {
		'Ogg (oga)': audioElement.canPlayType('audio/oga') + audioElement.canPlayType('audio/ogg'),
		'WebM':      audioElement.canPlayType('audio/webma') + audioElement.canPlayType('audio/webm'),
		'WAV':       audioElement.canPlayType('audio/wav') + audioElement.canPlayType('audio/wave')
	};
	
	var codecs = {
		'Vorbis': audioElement.canPlayType('audio/vorbis') + audioElement.canPlayType('audio/oga; codecs="vorbis"') + audioElement.canPlayType('audio/ogg; codecs="vorbis"'),
		'Opus':   audioElement.canPlayType('audio/opus') + audioElement.canPlayType('audio/oga; codecs="opus"')   + audioElement.canPlayType('audio/ogg; codecs="opus"'),
		'Speex':  audioElement.canPlayType('audio/speex') + audioElement.canPlayType('audio/x-speex') + audioElement.canPlayType('audio/oga; codecs="speex"') + audioElement.canPlayType('audio/oga; codecs="x-speex"') + audioElement.canPlayType('audio/ogg; codecs="speex"') + audioElement.canPlayType('audio/ogg; codecs="x-speex"'),
		'FLAC':   audioElement.canPlayType('audio/flac') + audioElement.canPlayType('audio/oga; codecs="flac"')   + audioElement.canPlayType('audio/ogg; codecs="flac"'),
		'PCM':    audioElement.canPlayType('audio/pcm') + audioElement.canPlayType('audio/wav; codecs="pcm"'),
		'MP3':    audioElement.canPlayType('audio/mp3') + audioElement.canPlayType('audio/mpeg'),
		'AAC':    audioElement.canPlayType('audio/aac') + audioElement.canPlayType('audio/mp4a') + audioElement.canPlayType('audio/mp4')
	};
	
	makeListCompatibilityMoreUserFriendly(containers);
	makeListCompatibilityMoreUserFriendly(codecs);
	
	body.innerHTML += '<h2 id="container">Container formats</h2>';
	body.innerHTML += getListCompatibility(containers, false);
	
	body.innerHTML += '<h2 id="codec">Codec formats</h2>';
	body.innerHTML += getListCompatibility(codecs, false);
}
