/*
 * Web Device Information
 * Copyright (C) 2014  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/* Requires
 * string-functions (String.prototype.includes)
 */
 

/**
 * @param {Array} array
 */
function makeListCompatibilityMoreUserFriendly(array)
{
	if(array == undefined)
		throw new TypeError('array is needed');
	
	for (var arrayKey in array)
	{
		if(array[arrayKey] == '')
			array[arrayKey] = 'no';
		else if(array[arrayKey].includes('probably'))
			array[arrayKey] = 'yes';
		else if(array[arrayKey].includes('maybe'))
			array[arrayKey] = 'unknown';
	}
}

/**
 * @param {Array} array
 * @param {boolean} returnDOM Return DOM (true) or string (false)
 * @return {(Element|string)} The list compatibility corresponding to the array parameter
 */
function getListCompatibility(array, returnDOM)
{
	if(array == undefined)
		throw new TypeError('array is needed');
	
	var list = document.createElement('ul');
	
	for (var arrayKey in array)
	{
		var element = document.createElement('li');
		element.innerHTML = arrayKey +' : '+ array[arrayKey];
		list.appendChild(element);
	}
	
	return returnDOM == undefined || returnDOM
		? list
		: list.outerHTML;
}
