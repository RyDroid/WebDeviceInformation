/*
 * Web Device Information
 * Copyright (C) 2014  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


var section = document.querySelector('main');
var videoElement = document.createElement('video');

if(typeof(videoElement.canPlayType) == 'function')
{
	var containers = {
		'Ogg (ogv)': videoElement.canPlayType('video/ogv') + videoElement.canPlayType('video/ogg'),
		'WebM':      videoElement.canPlayType('video/webmv') + videoElement.canPlayType('video/webm'),
		'MP4':       videoElement.canPlayType('video/m4v') + videoElement.canPlayType('video/mp4')
	};
	
	var codecs = {
		'Theora': videoElement.canPlayType('video/ogg; codecs="theora"') + videoElement.canPlayType('video/webm; codecs="theora"'),
		'VP8':    videoElement.canPlayType('video/ogg; codecs="vp8"')    + videoElement.canPlayType('video/webm; codecs="vp8"'),
		'VP9':    videoElement.canPlayType('video/ogg; codecs="vp9"')    + videoElement.canPlayType('video/webm; codecs="vp9"'),
		'H264':   videoElement.canPlayType('video/mp4; codecs="h264"'),
		'H265':   videoElement.canPlayType('video/mp4; codecs="h265"')
	};
	
	makeListCompatibilityMoreUserFriendly(containers);
	makeListCompatibilityMoreUserFriendly(codecs);
	
	section.innerHTML += '<h2 id="container">Container formats</h2>';
	section.innerHTML += getListCompatibility(containers, false);
	
	section.innerHTML += '<h2 id="codec">Codec formats</h2>';
	section.innerHTML += getListCompatibility(codecs, false);
}
else
{
	section.innerHTML = '<p>Video seems to be not supported.</p>';
}
