/*
 * Copyright (C) 2014  Nicola Spanti (also known as RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


if (!String.prototype.contains)
{
	if(typeof(String.prototype.includes) == 'function')
		String.prototype.contains = String.prototype.includes;
	else
	{
		/**
		 * @param {string} searchString
		 * @param {integer} position
		 * @return {boolean} true if searchString is a part of the string object (like "LIKE %searchString%" in SQL), otherwise false
		 */
		String.prototype.contains = function()
		{
			'use strict';
			return String.prototype.indexOf.apply(this, arguments) !== -1;
		};
	}
}
if (!String.prototype.includes)
	String.prototype.includes = String.prototype.contains;

if (!String.prototype.trim)
{
	/**
	 * Removes whitespace from both ends of the string
	 * @return {String} string trimmed
	 */
	String.prototype.trim = function()
	{
		'use strict';
		return this.replace(/^\s+|\s+$/g, '');
	};
}

/**
 * Check if a variable is a string.
 * @param {*} variable A variable to evaluate
 * @return {boolean} True if the variable is a string and false otherwise
 */
function isString(variable)
{
	'use strict';
	return typeof(variable) == 'string' || variable instanceof String;
}
