/*
 * Copyright (C) 2014-2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


if(typeof(getGamepads) != 'function')
{
	/**
	 * @return {Array} Array of gamepads
	 */
	function getGamepads()
	{
		'use strict';
		return typeof(navigator.getGamepads) == 'function' ? navigator.getGamepads() : [];
	}
}

/**
 * @param {*} variable A variable to evaluate
 * @return {boolean} True if the variable is a gamepad and false otherwise
 */
function isGamepad(variable)
{
	'use strict';
	return typeof(Gamepad) == 'function' && variable instanceof Gamepad;
}

/**
 * @param {*} variable A variable to evaluate
 * @return {boolean} True if the variable is a gamepad button and false otherwise
 */
function isGamepadButton(variable)
{
	'use strict';
	return typeof(GamepadButton) == 'function' && variable instanceof GamepadButton;
}

/**
 * @param {{Array|GamepadArray|GamepadList}} variable A variable to evaluate
 * @return {boolean} True if the variable is an empty array of gamepad and false otherwise
 */
function isGamepadArrayEmpty(gamepads)
{
	'use strict';
	
	if(typeof(gamepads) == 'undefined' ||
	   typeof(gamepads.length) == 'undefined' || gamepads.length == 0 ||
	   typeof(Gamepad) != 'function')
	{
		return true;
	}
	
	for(var i=0; i < gamepads.length; ++i)
	{
		if(gamepads[i] instanceof Gamepad)
			return false;
	}
	return true;
}

function isGamepadWithButtons(gamepad)
{
	'use strict';
	return isGamepad(gamepad) &&
	       typeof(gamepad.buttons) != 'undefined' && typeof(gamepad.buttons.length) == 'number' && gamepad.buttons.length > 0
}

function isGamepadWithStandardMapping(gamepad)
{
	'use strict';
	// https://w3c.github.io/gamepad/#remapping
	return isGamepadWithButtons(gamepad) &&
	       gamepad.buttons.length > 16 &&
	       isString(gamepad.mapping) && gamepad.mapping == 'standard';
}

function isXboxGamepadUnsafe(gamepad)
{
	'use strict';
	
	if(!isString(gamepad.id))
		return false;
	
	var simplifiedId = gamepad.id;
	simplifiedId = simplifiedId.toLowerCase();
	simplifiedId = simplifiedId.replace('-','');
	simplifiedId = simplifiedId.replace(' ','');
	return simplifiedId.includes('xbox') ||
	       (simplifiedId.includes('microsoft') &&
	        simplifiedId.includes('45e')       &&
	        simplifiedId.includes('28e'));
}

function isXboxGamepad(gamepad)
{
	'use strict';
	return isGamepad(gamepad) &&
	       isXboxGamepadUnsafe(gamepad);
}

function isPlayStationGamepadUnsafe(gamepad)
{
	'use strict';
	return isString(gamepad.id) &&
	       gamepad.id.toLowerCase().replace('-','').replace(' ','').includes('playstation');
}

function isPlayStationGamepad(gamepad)
{
	'use strict';
	return isGamepad(gamepad) &&
	       isPlayStationGamepadUnsafe(gamepad);
}
