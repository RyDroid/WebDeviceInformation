/*
 * Copyright (C) 2015  Nicola Spanti (RyDroid) <dev@nicola-spanti.info>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


var GAMEPAD_BUTTONS_MAPPING =
{
	"standard":
	{
		"left-directional-pad":
		{
			"left"  : 14,
			"right" : 15,
			"up"    : 12,
			"down"  : 13
		},
		"right-circles":
		{
			"left"  :  2,
			"□"     :  2,
			"X"     :  2,
			"right" :  1,
			"B"     :  1,
			"○"     :  1,
			"up"    :  3,
			"Y"     :  3,
			"△"     :  3,
			"down"  :  0,
			"A"     :  0,
			"❌"     :  0
		},
		"triggers":
		{
			"left"  : [4, 6],
			"right" : [5, 7]
		},
		"sticks":
		{
			"left"  : 10,
			"right" : 11
		},
		"others":
		{
			"back"  :  8,
			"select":  8,
			"foward":  9,
			"start" :  9,
			"home"  : 16
		}
	}
};


function getGamepadButtonsForLeftDirectionalPadUnsafe(gamepad)
{
	'use strict';
	return {
		'left' : gamepad.buttons[GAMEPAD_BUTTONS_MAPPING.standard["left-directional-pad"].left ],
		'right': gamepad.buttons[GAMEPAD_BUTTONS_MAPPING.standard["left-directional-pad"].right],
		'up'   : gamepad.buttons[GAMEPAD_BUTTONS_MAPPING.standard["left-directional-pad"].up   ],
		'down' : gamepad.buttons[GAMEPAD_BUTTONS_MAPPING.standard["left-directional-pad"].down ]
	};
}

function getGamepadButtonsForLeftDirectionalPad(gamepad)
{
	'use strict';
	return isGamepadWithStandardMapping(gamepad)
		? getGamepadButtonsForLeftDirectionalPadUnsafe(gamepad)
		: null;
}

function getGamepadButtonsForRightCirclesUnsafe(gamepad)
{
	'use strict';
	return {
		'left' : gamepad.buttons[GAMEPAD_BUTTONS_MAPPING.standard["right-circles"].left ],
		'right': gamepad.buttons[GAMEPAD_BUTTONS_MAPPING.standard["right-circles"].right],
		'up'   : gamepad.buttons[GAMEPAD_BUTTONS_MAPPING.standard["right-circles"].up   ],
		'down' : gamepad.buttons[GAMEPAD_BUTTONS_MAPPING.standard["right-circles"].down ]
	};
}

function getGamepadButtonsForRightCircles(gamepad)
{
	'use strict';
	return isGamepadWithStandardMapping(gamepad)
		? getGamepadButtonsForRightCirclesUnsafe(gamepad)
		: null;
}

function getGamepadButtonForLeftStickUnsafe(gamepad)
{
	'use strict';
	return gamepad.buttons[GAMEPAD_BUTTONS_MAPPING.standard.sticks.left];
}

function getGamepadButtonForLeftStick(gamepad)
{
	'use strict';
	return isGamepadWithStandardMapping(gamepad)
		? getGamepadButtonForLeftStickUnsafe(gamepad)
		: null;
}

function getGamepadButtonForRightStickUnsafe(gamepad)
{
	'use strict';
	return gamepad.buttons[GAMEPAD_BUTTONS_MAPPING.standard.sticks.right];
}

function getGamepadButtonForRightStick(gamepad)
{
	'use strict';
	return isGamepadWithStandardMapping(gamepad)
		? getGamepadButtonForRightStickUnsafe(gamepad)
		: null;
}

function getGamepadButtonsForSticksUnsafe(gamepad)
{
	'use strict';
	return {
		'left' : gamepad.buttons[GAMEPAD_BUTTONS_MAPPING.standard.sticks.left],
		'right': gamepad.buttons[GAMEPAD_BUTTONS_MAPPING.standard.sticks.right]
	};
}

function getGamepadButtonsForSticks(gamepad)
{
	'use strict';
	return isGamepadWithStandardMapping(gamepad)
		? getGamepadButtonsForSticksUnsafe(gamepad)
		: null;
}

function getGamepadButtonForStartUnsafe(gamepad)
{
	'use strict';
	return gamepad.buttons[GAMEPAD_BUTTONS_MAPPING.standard.others.start];
}

function getGamepadButtonForStart(gamepad)
{
	'use strict';
	return isGamepadWithStandardMapping(gamepad)
		? getGamepadButtonForStartUnsafe(gamepad)
		: null;
}

var getGamepadButtonForFowardUnsafe = getGamepadButtonForStartUnsafe;
var getGamepadButtonForFoward       = getGamepadButtonForStart;

function getGamepadButtonForSelectUnsafe(gamepad)
{
	'use strict';
	return gamepad.buttons[GAMEPAD_BUTTONS_MAPPING.standard.others.select];
}

function getGamepadButtonForSelect(gamepad)
{
	'use strict';
	return isGamepadWithStandardMapping(gamepad)
		? getGamepadButtonForSelectUnsafe(gamepad)
		: null;
}

var getGamepadButtonForBackUnsafe = getGamepadButtonForBackUnsafe;
var getGamepadButtonForBack       = getGamepadButtonForBack;

function getGamepadButtonForHomeUnsafe(gamepad)
{
	'use strict';
	return gamepad.buttons[GAMEPAD_BUTTONS_MAPPING.standard.others.home];
}

function getGamepadButtonForHome(gamepad)
{
	'use strict';
	return isGamepadWithStandardMapping(gamepad)
		? getGamepadButtonForHomeUnsafe(gamepad)
		: null;
}

function getGamepadButtonsForLeftTriggersUnsafe(gamepad)
{
	'use strict';
	return [
		gamepad.buttons[GAMEPAD_BUTTONS_MAPPING.standard.triggers.left[0]],
		gamepad.buttons[GAMEPAD_BUTTONS_MAPPING.standard.triggers.left[1]]
	];
}

function getGamepadButtonsForLeftTriggers(gamepad)
{
	'use strict';
	return isGamepadWithStandardMapping(gamepad)
		? getGamepadButtonsForLeftTriggersUnsafe(gamepad)
		: null;
}

function getGamepadButtonsForRightTriggersUnsafe(gamepad)
{
	'use strict';
	return [
		gamepad.buttons[GAMEPAD_BUTTONS_MAPPING.standard.triggers.right[0]],
		gamepad.buttons[GAMEPAD_BUTTONS_MAPPING.standard.triggers.right[1]]
	];
}

function getGamepadButtonsForRightTriggers(gamepad)
{
	'use strict';
	return isGamepadWithStandardMapping(gamepad)
		? getGamepadButtonsForRightTriggersUnsafe(gamepad)
		: null;
}

function getGamepadButtonsForTriggersUnsafe(gamepad)
{
	'use strict';
	return {
		'left' : getGamepadButtonsForLeftTriggersUnsafe (gamepad),
		'right': getGamepadButtonsForRightTriggersUnsafe(gamepad)
	};
}

function getGamepadButtonsForTriggers(gamepad)
{
	'use strict';
	return isGamepadWithStandardMapping(gamepad)
		? getGamepadButtonsForTriggersUnsafe(gamepad)
		: null;
}
